var app = {
    // Application Constructor
    initialize: function() {
        console.log('Binding events');
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        console.log('Device ready fired.');

        document.addEventListener('online', function() { alert("online mode."); }, false);
        document.addEventListener('offline', function() { alert("offline mode."); }, false);
        document.addEventListener('pause', function() { console.log("Going to background"); }, false);

        document.addEventListener('resume', function() {
            console.log("Resuming...");

            cordova.plugins.CorHttpd.stopServer(function() {

                cordova.plugins.CorHttpd.startServer({
                    'www_root': 'content-sync',
                    'port': 8080,
                    'localhost_only': false,
                    'error_page': '404.html'
                }, function(url) {
                    console.log('HTTPD started.');
                }, function(error) {
                    console.log(error);
                });
            }, function(error) {
                console.log('Error stopping server.');
                console.log(error);
            });

        }, false);

        // bind PDF anchors
        var pdfAnchors = document.querySelectorAll('a[href$=".pdf"]');
        console.log('Binding to ' + pdfAnchors.length + ' elements for PDF hijack');

        for (var i = 0; i < pdfAnchors.length; i++) {
            pdfAnchors[i].addEventListener('click', function(event) {
                var url = this.href;
                var title = this.title;

                openPDF(url, title);
                event.preventDefault();
            });
        }
    }
};

app.initialize();

function openPDF(url, title) {
    console.log('Attempting to open PDF.', url, title);

    var options = {
        title: title,
        documentView: { closeLabel: 'Close' },
        navigationView: { closeLabel: 'Close' },
        email: { enabled: true },
        print: { enabled: true },
        openWith: { enabled: true },
        bookmarks: { enabled: false },
        search: { enabled: false },
        autoClose: { onPause: false }
    };

    var isRemote = url.indexOf('file://') < 0;

    console.log('Is file remote?', isRemote);


    if (isRemote) {
        // consider enhancing this... or just read from synced-content
        var tmpPDF = cordova.file.tempDirectory + 'tmp.pdf';

        var ft = new FileTransfer();
        window.console.log("transfering source " + url + " to " + tmpPDF);
        ft.download(url, tmpPDF,
            function(entry) {
                console.log('Download succeeded, opening file in PDF viewer.');
                cordova.plugins.SitewaertsDocumentViewer.viewDocument(tmpPDF, 'application/pdf', options, null,
                    function() {
                        window.resolveLocalFileSystemURL(cordova.file.tempDirectory, function(dir) {
                            dir.getFile('tmp.pdf', { create: false }, function(fileEntry) {
                                fileEntry.remove(function() {
                                    console.log('cleanup success');
                                }, function(error) {
                                    console.log('cleanup failed, oh well...');
                                }, function() {
                                    console.log('no cleanup required');
                                });
                            });
                        });
                    });
            },
            function(err) {
                console.log("Error");
                console.dir(err);
            });
    } else {
        cordova.plugins.SitewaertsDocumentViewer.viewDocument(url, 'application/pdf', options);
    }
}