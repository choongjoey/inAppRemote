/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

var appsync = false;
var contentsync = false;

function prepareOfflineFolder() {
    if (appsync === true && contentsync === true) {
        console.log('Preparing offline folder structure.');

        function errorHandler(e) {
            console.log(e);
        }

        window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function(dataDir) {
            console.log(dataDir);

            // try to see if it exists...
            dataDir.getDirectory('offline-www', {}, function(offlineWwwFolder) {
                //exists, so we kasi delete.
                offlineWwwFolder.removeRecursively(function() {
                    console.log('delete old offline-www');
                    moveStuff();
                }, errorHandler);
                moveStuff()
            }, function(e) {
                //empty so we can proceed
                moveStuff();
            });


            function moveStuff() {
                dataDir.getDirectory('offline-www', { create: true }, function(offlineWwwFolder) {
                    console.log('Created offline-www folder');
                    dataDir.getDirectory('my-prs/my-prs/en', {}, function(appFolder) {
                        appFolder.moveTo(offlineWwwFolder, 'en', function() {
                            console.log('my-prs/en folder moved.');
                            offlineWwwFolder.getDirectory('content', { create: true }, function(appContentFolder) {
                                console.log('content folder created.');
                                dataDir.getDirectory('dam/content/dam', {}, function(damFolder) {
                                    damFolder.moveTo(appContentFolder, 'dam', function() {
                                        console.log('dam moved.');
                                        dataDir.getDirectory('dam/content/etc', {}, function(etcFolder) {
                                            etcFolder.moveTo(offlineWwwFolder, 'etc', function() {
                                                console.log('etc moved.');

                                                alert('Offline sync complete.');
                                                cordova.InAppBrowser.open('https://api.repulze.com/cordova-test/test.html', '_self');
                                            }, errorHandler);
                                        }, errorHandler);
                                    }, errorHandler);
                                }, errorHandler);
                            }, errorHandler);
                        }, errorHandler);
                    }, errorHandler);
                }, errorHandler);
            }
        });
    } else {
        return;
    }
}

var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');

        navigator.notification.alert('Hello..', null, 'The Other Side', 'Its me.');

        function onDeviceOnline() {
            console.log('Online');
            // do your content syncing here...
            // assume everything is stored in /content-sync

            var sync = ContentSync.sync({
                src: 'https://api.repulze.com/cordova-test/my-prs.zip',
                id: 'my-prs'
            });

            sync.on('progress', function(data) { console.log(data.progress); });
            sync.on('error', function(e) { console.log(e); });

            sync.on('complete', function(data) {
                window.resolveLocalFileSystemURL("file://" + data.localPath, function(entry) {
                    console.log(entry);
                    appsync = true;
                    prepareOfflineFolder();
                }, function(error) {
                    console.log("Error: " + error.code);
                });
            });

            var damSync = ContentSync.sync({
                src: 'https://api.repulze.com/cordova-test/content.zip',
                id: 'dam'
            });

            damSync.on('progress', function(data) { console.log(data.progress); });
            damSync.on('error', function(e) { console.log(e); });

            damSync.on('complete', function(data) {
                window.resolveLocalFileSystemURL("file://" + data.localPath, function(entry) {
                    console.log(entry);
                    contentsync = true;
                    prepareOfflineFolder();
                }, function(error) {
                    console.log("Error: " + error.code);
                });
            });
        }

        //document.addEventListener('online', onDeviceOnline, false);
        //document.addEventListener('offline', function() {
        console.log('Offline mode.');
        // do your checks, open offline iab.
        // assume everything is stored in /content-sync
        // start local HTTPD

        var httpd = cordova.plugins.CorHttpd;

        // var www_root = cordova.file.dataDirectory;
        // www_root = www_root.substring(7, www_root.length) + 'offline-www';

        var www_root = 'content-sync';

        console.log('Starting HTTPD at ' + www_root);

        httpd.startServer({
                'www_root': www_root,
                'port': 8080,
                'localhost_only': false,
                'error_page': '404.html'
            }, function(url) {
                console.log('HTTPD started.');

                //cordova.InAppBrowser.open('content-sync/test.html', '_self');
                var offlineIAB = cordova.InAppBrowser.open('http://localhost:8080/index.html', '_self', 'clearcache=yes');

                offlineIAB.addEventListener('exit', function() {
                    httpd.stopServer(function() {
                            console.log('HTTPD stopped.')
                        },
                        function(e) {
                            console.log(e);
                        });
                });
            },
            function(error) {
                console.log(error);
            });

        //}, false);

        // var networkState = navigator.connection.type;
        // console.log('Connection type: ' + networkState);
        // if (networkState !== Connection.NONE) {
        //     onDeviceOnline();
        // }
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

app.initialize();